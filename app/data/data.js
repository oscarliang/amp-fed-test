var data = [
    {
        'id': '1',
        'firstName': 'Sean',
        'lastName': 'Kerr',
        'picture': 'images/sean.png',
        'Title': 'Senior Developer'
    },
    {
        'id': '2',
        'firstName': 'Yaw',
        'lastName': 'Ly',
        'picture': 'images/yaw.png',
        'Title': 'AEM Magician'
    },
    {
        'id': '3',
        'firstName': 'Lucy',
        'lastName': 'Hehir',
        'picture': 'images/lucy.png',
        'Title': 'Scrum master'
    },
    {
        'id': '4',
        'firstName': 'Rory',
        'lastName': 'Elrick',
        'picture': 'images/rory.png',
        'Title': 'AEM Guru'
    },
    {
        'id': '5',
        'firstName': 'Eric',
        'lastName': 'Kwok',
        'picture': 'images/eric.png',
        'Title': 'Technical Lead'
    },
    {
        'id': '6',
        'firstName': 'Hayley',
        'lastName': 'Crimmins',
        'picture': 'images/hayley.png',
        'Title': 'Dev Manager'
    }
];