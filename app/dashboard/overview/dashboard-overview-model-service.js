// Factory
app.factory('OverviewModel', function(userApi) {
	// Define the constructor function.

	function OverviewModel() {
		_self = this;
	}
	// Define the "instance" methods using the prototype
	// and standard prototypal inheritance.
	OverviewModel.prototype = {
                getUserList: function(){
                   return userApi.getUserList()
                }
	};
	return OverviewModel;
});