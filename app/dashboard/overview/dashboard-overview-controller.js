angular.module('ampUI').controller('OverviewController', function ($rootScope, $scope, OverviewModel) {
    Overview = new OverviewModel();

    OverviewController = function () {
        $scope.model = Overview;
        $scope.userList = Overview.getUserList();
    };
    OverviewController();
});